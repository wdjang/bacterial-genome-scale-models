# Bacterial genome-scale metabolic models constructed by MBEL at KAIST #

### List of bacterial genome-scale models ###

1. *Mannheimia succiniciproducens* MBEL55E (MsuMBEL686) [Kim *et al.*, Biotechnol. Bioeng. (2007)](http://onlinelibrary.wiley.com/doi/10.1002/bit.21433/abstract;jsessionid=BDD34095992F5B1A232576CF1F8C461E.f02t01)  
2. *Acinetobacter baumannii* AYE (AbyMBEL891) [Kim *et al.*, Mol. Biosyst. (2010)](http://pubs.rsc.org/en/Content/ArticleLanding/2010/MB/B916446D#!divAbstract)  
3. *Pseudomonas putida* KT2440 (PpuMBEL1071) [Sohn *et al.*, Biotechnol. J. (2010)](http://onlinelibrary.wiley.com/doi/10.1002/biot.201000124/abstract)  
4. *Zymomonas mobilis* ZM4 (ZmoMBEL601) [Lee *et al.*, Microb. Cell Fact. (2010)](https://microbialcellfactories.biomedcentral.com/articles/10.1186/1475-2859-9-94)  
5. *Vibrio vulnificus* CMCP6 (VvuMBEL943) [Kim *et al.*, Mol. Syst. Biol. (2011)](http://msb.embopress.org/content/7/1/460#sec-20)  
6. *Ralstonia eutropha* H16 (RehMBEL1391) [Park *et al.*, BMC Sys. Biol. (2011)](https://bmcsystbiol.biomedcentral.com/articles/10.1186/1752-0509-5-101)  